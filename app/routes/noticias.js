module.exports = function(application) { 

	application.get('/noticias', function(req, res){

		var connection = application.config.dbconnections();
		var noticiasModel = application.app.models.noticiasmodel;

		noticiasModel.getNoticias(connection, function(error, result){
			res.render("noticias/noticias", {noticias : result}); 
		});	
	});
};