module.exports = function(application) { 

	application.get('/noticia', function(req, res){

		var connection = application.config.dbconnections();
		var noticiasModel = application.app.models.noticiasmodel;


		noticiasModel.getNoticia(connection, function(error, result){
			res.render("noticias/noticia", {noticia : result});
		});	
	});
};